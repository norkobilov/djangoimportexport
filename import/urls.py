from django.urls import path
from .views import import_data, export_data

urlpatterns = [
    path('import/', import_data, name='import'),
    path('export/', export_data, name='export'),
]

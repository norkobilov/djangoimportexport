import io
from io import BytesIO

from django.core.files.base import ContentFile
from django.http import HttpResponse, FileResponse
from django.shortcuts import render
from django.template.loader import get_template
from django.views import View
from django.views.generic import ListView, TemplateView
from reportlab.pdfgen import canvas
from tablib import Dataset
from .models import Person

from .resources import PersonResource
from xhtml2pdf import pisa


def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type="application/pdf")
    return None


def export_data(request):
    if request.method == 'POST':
        # Get selected option from form
        file_format = request.POST['file-format']
        employee_resource = PersonResource()
        dataset = employee_resource.export()
        if file_format == 'CSV':
            response = HttpResponse(dataset.csv, content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="exported_data.csv"'
            return response
        elif file_format == 'JSON':
            response = HttpResponse(dataset.json, content_type='application/json')
            response['Content-Disposition'] = 'attachment; filename="exported_data.json"'
            return response
        elif file_format == 'XLS (Excel)':
            response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename="exported_data.xls"'
            return response
        elif file_format == 'PDF':
            persons = Person.objects.all()
            print(persons)
            context = {'persons': persons}
            pdf = render_to_pdf('pdf.html', context)
            if pdf:
                response = HttpResponse(pdf, content_type='application/pdf')
                content = 'inline; filename=contact.pdf'
                response['Content-Disposition'] = content
                return response
    return render(request, 'export.html')


def import_data(request):
    if request.method == 'POST':
        file_format = request.POST['file-format']
        employee_resource = PersonResource()
        dataset = Dataset()
        new_employees = request.FILES['importData']

        if file_format == 'CSV':
            imported_data = dataset.load(new_employees.read().decode('utf-8'),format='csv')
            result = employee_resource.import_data(dataset, dry_run=True)
        elif file_format == 'JSON':
            imported_data = dataset.load(new_employees.read().decode('utf-8'),format='json')
            # Testing data import
            result = employee_resource.import_data(dataset, dry_run=True)
        elif file_format == 'Excel':
            imported_data = dataset.load(new_employees.read(),format='xls')
            # Testing data import
            result = employee_resource.import_data(dataset, dry_run=True)

        if not result.has_errors():
            # Import now
            employee_resource.import_data(dataset, dry_run=False)

    return render(request, 'import.html')
